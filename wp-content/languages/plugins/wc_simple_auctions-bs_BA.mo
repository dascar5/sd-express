��    #      4      L      L  2   M  3   �  4   �     �     �               )     9     L     \     n     r     w  2   |     �     �     �     �     �     �     �  %   �            T   !  -   v  A   �  
   �     �     �                 .       B  7   T  8   �     �     �     �     �                5     H  	   ]     g     m  C   r     �     �     �     �     �     �     �  ;   �     0	     8	  \   @	  +   �	  L   �	     
     %
     8
     D
     M
     V
   <span class="current auction">Current bid:</span>  <span class="current auction">Starting bid:</span>  <span class="starting auction">Starting bid:</span>  Add to watchlist! Auction History Auction ends: Auction history Auction started Auction starts in: Auction starts: Auctions settings Bid Date Days Get email notification for my auctions ending soon Hour Hours Item condition Item condition: Minute Minutes New No need to bid. Your bid is winning!  Second Seconds Successfully placed a bid for &quot;%s&quot; but it does not meet the reserve price! Successfully placed a bid for &quot;%s&quot;! Successfully placed a bid for &quot;%s&quot;! Your max bid is %s. Time left: Timezone: %s Used User Week Weeks Project-Id-Version: WooCommerce Simple Auction 1.2.39
Report-Msgid-Bugs-To: http://wordpress.org/tag/woocommerce-simple-auctions
POT-Creation-Date: 2020-09-28 09:40:50+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-12-23 12:47+0000
Last-Translator: SD Express
Language-Team: Bosanski
Language: bs_BA
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.6; wp-5.6 Trenutna cijena:  <span class="current auction">Početna cijena: </span>  <span class="starting auction">Početna cijena: </span>  Dodaj na posmatranje! Istorija aukcije Aukcija završava: Istorija aukcije Aukcija počela Aukcija počinje za: Aukcija počinje:  Podešavanja aukcija Licitiraj Datum Dana Dobij email notifikacije za moje aukcije koje se završavaju uskoro Sat Sati Stanje proizvoda Stanje proizvoda: Minut Minuta Novo Nema potrebe za licitiranjem. Vaša licitacija je najveća! Sekundu Sekundi Uspješno ste licitirali na &quot;%s&quot; ali to nije dovoljno za minimalnu cijenu prodaje! Uspješno ste licitirali na &quot;%s&quot;! Uspješno ste licitirali na &quot;%s&quot;! Vaša najveća licitacija je %s. Završava za:  Vremenska zona: %s Korišćeno Korisnik Nedjelja Nedjelja 