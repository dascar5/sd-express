��    R      �      <      <  *   =     h     }     �     �     �     �  
   �     �     �     �               )     5     ;  =   D  ,   �  m   �  s     )   �  '   �  '   �  !     &   -     T     r     ~  
   �     �     �     �     �     �     �     �     �  	   �     	     	     &	     4	  
   G	     R	     Z	     q	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     
  '   
  
   G
     R
     b
     z
     �
     �
     �
     �
     �
     �
     �
     �
  8   �
  	        &     2     P  !   \  
   ~     �     �  G  �     �     �     �               '     <  	   E     O     \     |     �     �     �     �     �  7   �  0   
  l   ;  r   �  0     )   L  *   v  '   �  &   �     �       
             -     5     E     U     k     z     �     �  
   �     �     �     �     �  	   �     �               6  
   <     G     N     S  	   i     s     z     �     �     �     �     �  )   �  	             0  	   M     W     d     r     �     �     �     �     �  ,   �  
   �     �     	     $     2     O     b     k   %1$s installed and activated successfully. %d review %d reviews %s (Invalid) %s (Pending) %s Categories %s for %s item(s) %s of %s %s overall %s quantity &copy; %s - All Rights Reserved &larr; Older Comments &larr; Previous + Show more - Show less -65%f -80% off ...and receive <strong>$20 coupon for first shopping</strong> <span class="highlight">Special</span> Offer <span class="upto"><span class="prefix">Upto</span><span class="value">70</span><span class="suffix">%</span> <span class="upto"><span class="prefix">Upto</span><span class="value">70</span><span class="suffix"></span></span> <strong>365 days</strong> for free return <strong>99% Positive</strong> Feedbacks <strong>Free Delivery</strong> from $50 <strong>Only Best</strong> Brands <strong>Payment</strong> Secure System A list of product categories. Accessories Activate Add Review Added All Categories All Departments All Departments Menu All Products All Smartphones Already Sold: Amount Animation Attributes Title Availability Availability: Available version: Available: Average Awaiting product image Best Selling Products Blog Blog Menu Blue Bottom Browse Categories Cancel Cart Catalog Mode Catch Daily Deals! Categories Compare Display Attributes Enter your email address Manage specifications for this product. My Account Onsale Products Return to the Dashboard Reviews Sign Up Specification Specifications Top Rated Products Wishlist WordPress Repository Yellow Yes You must be <a href="%s">logged in</a> to post a review. Your Logo Your Rating Your Recently Viewed Products Your Review Your comment is awaiting approval Your order Youtube brand Project-Id-Version: Electro 2.6.1
Report-Msgid-Bugs-To: https://themeforest.net/item/electro-electronics-store-woocommerce-theme/15720624/support/
POT-Creation-Date: 2020-10-27 15:15:33+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-11-20 15:32+0000
Last-Translator: 
Language-Team: Српски језик
X-Generator: Loco https://localise.biz/
Language: sr_RS
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
X-Loco-Version: 2.4.4; wp-5.5.3 % %d recenzija  %d recenzija %s (Invalidno) %s (Čeka se) %s Kategorije %s za %s proizvod(a) %s od %s %s ukupno %s količina &copy; %s - Sva prava zadržana &larr; Stariji komentari &larr; Prethodno + Prikaži više - Prikaži manje -65% -80% ...i primi <strong>kupon u vrijednosti 20 eura</strong> <span class="highlight">Specijalna</span> ponuda <span class="upto"><span class="prefix">Do</span> <span class="value">70</span><span class="suffix">%</span> <span class="upto"><span class="prefix">Do</span> <span class="value">70</span><span class="suffix"></span></span> <strong>365 dana</strong> za besplatni povratak. <strong>99% Pozitivnih</strong> Recenzija <strong>Besplatna dostavka</strong> od 50e <strong>Samo najbolji</strong> brendovi <strong>Sigurni</strong> platni sistem Lista kategorija proizvoda
 Dodaci
 Aktiviraj
 Dodaj ocjenu
 Dodato
 Sve kategorije
 Svi departmani
 Meni svih departmana
 Svi proizvodi
 Svi pametni telefoni
 Prodato
 Vrijednost
 Animacija
 Naziv specifikacije Dostupnost
 Dostupnost: Dostupne verzije:
 Dostupno
 Prosjek
 Čekanje slike proizvoda
 Najprodavaniji proizvodi
 Blog
 Blog meni
 Plava
 Dno
 Pregledaj kategorije
 Poništi
 Korpa
 Režim kataloga
 Ulov
i ponudu dana!
 Kategorije
 Uporedi
 Prikaži specifikacije Unesite Vašu email adresu Upravljaj specifikacija za ovaj proizvod. Moj nalog Proizvodi na prodaji Povratak na kontrolnu tablu
 Recenzije Prijavite se Specifikacije Specifikacije Najbolje ocijenjeni Lista želja
 WordPress spremište
 Žuta
 Da
 Morate biti ulogovani da bi ostavili ocjenu
 Vaš logo
 Vaš rejting
 Nedavno gledani proizvodi
 Vaša ocjena
 Vaš komentar čeka potvrdu
 Vaša porudžbina
 Youtube
 brend
 