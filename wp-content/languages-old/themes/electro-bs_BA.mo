��    i      d      �      �  *   �     �     �     �               '  
   0     ;     G     g     }     �     �     �     �  =   �  ,   �  m     s   �  )   	  '   +	  '   S	  !   {	  &   �	     �	     �	     �	  
   �	     
     
     
     '
     <
     I
     Y
     g
  	   n
     x
     �
     �
     �
  
   �
     �
     �
  &   �
  '        0     F  	   K     U     Z     a     s     z          �  
   �     �     �  T   �          )     A  #   Z  '   ~  
   �     �     �     �     �     �     �          !     )     1  '   9     a     j     x     �     �     �  
   �  
   �     �     �       &        F     O     d     k  8   o  	   �     �     �     �  !   �  
   
               #  >  &     e     g     �     �     �     �     �  	   �     �     �               ,     =     N     S  7   X  0   �  l   �  r   .  0   �  )   �  *   �  '   '  &   O     v     �  
   �     �     �     �     �     �     �                  
   +     6     J     V     b  	   u          �  O   �  )   �          6  
   <     G     N     S  	   i     s     z     �     �     �     �  I   �          -     <  +   W  )   �  	   �     �     �     �     �     �          (  	   :     D     Q  &   ^     �     �     �  ,   �  
   �     �     �          '     <     P  .   o     �     �     �     �  ,   �  
   �               .     <     Y     l     u     |   %1$s installed and activated successfully. %d review %d reviews %s (Invalid) %s (Pending) %s Categories %s for %s item(s) %s of %s %s overall %s quantity &copy; %s - All Rights Reserved &larr; Older Comments &larr; Previous + Show more - Show less -65%f -80% off ...and receive <strong>$20 coupon for first shopping</strong> <span class="highlight">Special</span> Offer <span class="upto"><span class="prefix">Upto</span><span class="value">70</span><span class="suffix">%</span> <span class="upto"><span class="prefix">Upto</span><span class="value">70</span><span class="suffix"></span></span> <strong>365 days</strong> for free return <strong>99% Positive</strong> Feedbacks <strong>Free Delivery</strong> from $50 <strong>Only Best</strong> Brands <strong>Payment</strong> Secure System A list of product categories. Accessories Activate Add Review Added All Categories All Departments All Departments Menu All Products All Smartphones Already Sold: Amount Animation Attributes Title Availability Availability: Available version: Available: Average Awaiting product image Based on %s review Based on %s reviews Be the first to review &ldquo;%s&rdquo; Best Selling Products Blog Blog Menu Blue Bottom Browse Categories Cancel Cart Catalog Mode Catch Daily Deals! Categories Compare Contact Vendor Create new account today to reap the benefits of a personalized shopping experience. Display Attributes Don't have an account ? Enter your email address Keep a record of all your purchases Manage specifications for this product. My Account Name No ratings found yet! Onsale Products Owner of Store Register Return to the Dashboard Returning Customer ? Reviews Sign Up Sign in Sign up today and you will be able to : Since %s Specification Specifications Speed your way through checkout Store Category Store Closed Store Open Store Time There are no reviews yet. Top Rated Products Track your orders easily Welcome back! Sign in to your account. Wishlist WordPress Repository Yellow Yes You must be <a href="%s">logged in</a> to post a review. Your Logo Your Rating Your Recently Viewed Products Your Review Your comment is awaiting approval Your order Youtube brand or Project-Id-Version: Electro 2.6.1
Report-Msgid-Bugs-To: https://themeforest.net/item/electro-electronics-store-woocommerce-theme/15720624/support/
POT-Creation-Date: 2020-10-27 15:15:33+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-11-29 15:51+0000
Last-Translator: Bogdan
Language-Team: Bosanski
X-Generator: Loco https://localise.biz/
Language: bs_BA
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
X-Loco-Version: 2.4.4; wp-5.5.3 % %d recenzija  %d recenzija %s (Invalidno) %s (Čeka se) %s Kategorije %s za %s proizvod(a) %s od %s %s ukupno %s količina &copy; %s - Sva prava zadržana &larr; Stariji komentari &larr; Prethodno + Prikaži više - Prikaži manje -65% -80% ...i primi <strong>kupon u vrijednosti 20 eura</strong> <span class="highlight">Specijalna</span> ponuda <span class="upto"><span class="prefix">Do</span> <span class="value">70</span><span class="suffix">%</span> <span class="upto"><span class="prefix">Do</span> <span class="value">70</span><span class="suffix"></span></span> <strong>365 dana</strong> za besplatni povratak. <strong>99% Pozitivnih</strong> Recenzija <strong>Besplatna dostavka</strong> od 50e <strong>Samo najbolji</strong> brendovi <strong>Sigurni</strong> platni sistem Lista kategorija proizvoda
 Dodaci
 Aktiviraj
 Dodaj ocjenu
 Dodato
 Sve kategorije
 Svi departmani
 Meni svih departmana
 Svi proizvodi
 Svi pametni telefoni
 Prodato
 Vrijednost
 Animacija
 Naziv specifikacije Dostupnost
 Dostupnost: Dostupne verzije:
 Dostupno
 Prosjek
 Čekanje slike proizvoda
 Zasnovano na %s recenziji Zasnovano na %s recenzijama Zasnovano na %s recenzija Budite prvi da ocijenite &ldquo;%s&rdquo; Najprodavaniji proizvodi
 Blog
 Blog meni
 Plava
 Dno
 Pregledaj kategorije
 Poništi
 Korpa
 Režim kataloga
 Ulov
i ponudu dana!
 Kategorije
 Uporedi
 Kontaktirajte prodavca Napravite novi nalog danas da uživate u benefitima koje SD Express nudi. Prikaži specifikacije Nemate nalog ? Unesite Vašu email adresu Otvorite svoju radnju sa Vašim proizvodima Upravljaj specifikacija za ovaj proizvod. Moj nalog Ime Još uvijek nema recenzija! Proizvodi na prodaji Vlasnik radnje Registrujte se. Povratak na kontrolnu tablu
 Već imate nalog? Recenzije Prijavite se Ulogujte se. Registrujte se danas i moći ćete :   Od %s Specifikacije Specifikacije Brzinski i lako da kupite sve što poželite Kategorija Radnja je zatvorena trenutno. Radnja je otvorena trenutno Radno vrijeme Još nema recenzija. Najbolje ocijenjeni Pratite Vaše narudžbine lako Dobrodošli nazad! Prijavite se na svoj nalog. Lista želja
 WordPress spremište
 Žuta
 Da
 Morate biti ulogovani da bi ostavili ocjenu
 Vaš logo
 Vaš rejting
 Nedavno gledani proizvodi
 Vaša ocjena
 Vaš komentar čeka potvrdu
 Vaša porudžbina
 Youtube
 brend
 ili 