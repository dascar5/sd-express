[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![GPL License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/dascar5/SD-Express">
    <img src="images/logo.png" alt="Logo">
  </a>

  <p align="center">
    Complete Multi-Vendor E-Commerce platform made in Wordpress, fashioned after AliExpress and Amazon.
    <br />
    <br />
    <a href="https://www.sdexpress.me/">Check Website</a>
    ·
    <a href="https://github.com/dascar5/SD-Express/issues">Report Bug</a>
    ·
    <a href="https://github.com/dascar5/SD-Express/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#set-up">Set up</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

[![Product Name Screen Shot][product-screenshot]](https://www.sdexpress.me/)

Complete Multi-Vendor E-Commerce solution, containing all and any functions you might expect from one.

- Admin panel
- Product edits, attributes, auctions, multiple vendors, reviews, ratings, comments, live chat, etc.
- Easily customizable themes and plugins

### Built With

- [WordPress](https://wordpress.com/)
- [WooCommerce](https://woocommerce.com/)
- [Electro Electronics Store](https://themeforest.net/item/electro-electronics-store-woocommerce-theme/15720624)
- [Dokan Pro](https://wedevs.com/dokan)
- [Slider Revolution](https://www.sliderrevolution.com/)
- [YITH WooCommerce Compare](https://yithemes.com/themes/plugins/yith-woocommerce-compare/)
- [YITH WooCommerce Wishlist](https://yithemes.com/themes/plugins/yith-woocommerce-wishlist/)

<!-- GETTING STARTED -->

## Getting Started

Setting this up for yourself is really easy, as this huge repo includes all the plugins you need.

### Set up

You simply have to download this repo as a .zip, and upload it to your hosting service. Then update wp-config.php with your new server info. I personally use and recommend SiteGround, as they're super cheap and basic to set up.

<!-- USAGE EXAMPLES -->

## Usage

This is a general purpose Multi-Vendor E-Commerce platform. You can sell anything you want, disable the Multi-Vendor functionality if you want, and so much more. This repo does not come with products and images, you are going to have to add your own.

I recommend adding this custom CSS, as it fixes the overlap between Account Menu and the helper popup, something theme creators missed. I don't want to mess with the original theme files in this repo, that's why I'm posting it here.

```css
.site-header .dropdown-menu {
  z-index: 9999;
}
```

_If you need help with some minor fixes, please contact me @ admin@sdexpress.me_.

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->

## License

Distributed under the GNU General Public License v3.0. See `LICENSE.txt` for more information.

<!-- CONTACT -->

## Contact

Bogdan Laban - [@dante_vn](https://www.instagram.com/dante_vn/) - [email](admin@sdexpress.me)

Project Link: [SD Express](https://github.com/dascar5/SD-Express)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/github/contributors/dascar5/SD-Express.svg?style=for-the-badge
[contributors-url]: https://github.com/dascar5/SD-Express/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/dascar5/SD-Express.svg?style=for-the-badge
[forks-url]: https://github.com/dascar5/SD-Express/network/members
[stars-shield]: https://img.shields.io/github/stars/dascar5/SD-Express.svg?style=for-the-badge
[stars-url]: https://github.com/dascar5/SD-Express/stargazers
[issues-shield]: https://img.shields.io/github/issues/dascar5/SD-Express.svg?style=for-the-badge
[issues-url]: https://github.com/dascar5/SD-Express/issues
[license-shield]: https://img.shields.io/github/license/dascar5/SD-Express.svg?style=for-the-badge
[license-url]: https://github.com/dascar5/SD-Express/blob/main/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/bogdan-laban-521070163/
[product-screenshot]: images/screenshot.png
